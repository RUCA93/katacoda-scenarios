# Now we can put our changes into the remote repository

## Tasks

Let's proceed with this.  

`git add .`{{execute}}  

`git commit -m 'wrote a script I am quite proud of that everyone should see'`{{execute}}  

`git push origin add-another-script`{{execute}}  

And now imagine that we are in GitLab, let’s go ahead and merge our new branch into master.  
