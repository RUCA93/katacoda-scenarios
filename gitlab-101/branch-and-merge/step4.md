# Let’s add a new feature and write some actual code. To do this properly, we’ll create a new branch before adding the code

## Tasks

First we'll ad a new branch.  

`git checkout -b add-hello-world`{{execute}}  

And the a file with code in there.  

`echo "Hello, World!" > hello.txt`{{execute}}  

Let's run it!  

`cat hello.txt`{{execute}}  

Awesome, our amazing script works, we’ve added a new file, we already have new-file and new-file-2.
