# Over this scenario let's assume master branch is protected, so that means we can't push changes directly there

## Task

First let's make sure we are over our project folder.  

`cd /home/scrapbook/tutorial/git/my-local/project`{{execute}}  

We will find a previous file created.  

`ls`{{execute}}  

Now create a branch to work over there.  

`git checkout -b add-file-2`{{execute}}  

We can work over that branch, it will be our **feature** branch.  

`touch new-file-2`{{execute}}  
