# Let’s add our new script to the stage, commit, and push our branch

## Task

`git add hello.txt`{{execute}}  

`git commit -m 'adding in hello world script'`{{execute}}  

`git push origin add-hello-world`{{execute}}  

Origin has our new branch.
